#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#define MAX_TOKEN_LEN 32
#define MAX_PATH_NUM 32
#define MAX_ARG_NUM 20
#define MAX_CWD_LEN 128
#define MAX_MSG_LEN 128

char msg_buf[MAX_MSG_LEN];

struct cmd_line
{
	char *inf, *ouf;
	char **argv;
	int bkg;
	struct cmd_line *pipe; // next command after the pipe sign
};

void destructor(struct cmd_line *this)
{
	int i;
	free(this->inf);
	free(this->ouf);
	if (this->argv)
		for (i = 0; this->argv[i]; ++i) free(this->argv[i]);
	free(this->argv);
	if (this->pipe)
		destructor(this->pipe);
}

int check_char(char c)
{
	switch (c)
	{
		case '\n': return 0;
		case EOF : return -1;
		case '<' : return 2;
		case '>' : return 3;
		case '|' : return 4;
		default: return 1;
	}
}

int get_token(char *tok)
{
	char c, bflag = 0;
	int cflag;

	while ((c = getchar()) != EOF && isspace(c) && c!='\n') {}
	if (c == EOF)
		return -1;
	ungetc(c, stdin);

	while ((c = getchar()) != EOF)
	{
		if (!bflag)
		{
			*tok = 0;
			if ((cflag = check_char(c)) != 1 || isspace(c))
				return cflag;
		}
		if (c == '\\')
		{
			c = getchar();
			if (c == '\n')
				continue;
		}
		else if (c == '\"')
		{
			bflag ^= 1;
			continue;
		}
		*(tok++) = c;
	}
	return -1;
}

int set_argv(char *argv[MAX_ARG_NUM], char *tok, int *argc)
{
	int flag = get_token(tok);
	if (strlen(tok) > 0)
	{
		argv[*argc] = calloc((strlen(tok)+1), sizeof(char));
		strcpy(argv[(*argc)++], tok);
	}
	return flag;
}

void set_all_bkg(struct cmd_line* bot)
{
	while (bot)
	{
		bot->bkg = 1;
		bot = bot->pipe;
	}
}

void dump_msg()
{
	if (strlen(msg_buf) > 0)
	{
		printf("%s", msg_buf);
		msg_buf[0] = 0;
	}
}

struct cmd_line* get_tok_line()
{
	char tok[MAX_TOKEN_LEN] = "\0", *argv[MAX_ARG_NUM] = {0}, cwd[MAX_CWD_LEN], c;
	int argc = 0, flag;
	struct cmd_line *top = calloc(1, sizeof(struct cmd_line)), *bot = top, *cur = top;

	if (getcwd(cwd, sizeof(cwd)) == NULL)
		exit(EXIT_FAILURE);
	dump_msg();
	printf("%s shell: ", cwd);
	fflush(stdout);

	flag = set_argv(argv, tok, &argc);

	if (flag < 0)
		return NULL;

	if (flag > 0)
		do
		{
			if (flag > 1 && flag < 4) // redirection
			{
				int tmp = get_token(tok);
				if (strlen(tok)==0)
				{
					printf("Syntax error!\n");
					if (tmp > 0)
						while ((c=getchar())!=EOF && c!='\n') {}
					bot->argv[0][0] = 0;
					return bot;
				}
				if (flag == 2) // input
				{
					top->inf = calloc((strlen(tok)+1), sizeof(char));
					strcpy(top->inf, tok);
				}
				else // output
				{
					top->ouf = calloc((strlen(tok)+1), sizeof(char));
					strcpy(top->ouf, tok);
				}
				flag = tmp;
			}
			else if (flag == 4) // pipe sign
			{
				top->argv = calloc((argc+1), sizeof(char*));
				memcpy(top->argv, argv, (argc+1)*sizeof(char*));
				argc = 0;
				memset(argv, 0, sizeof(argv));
				cur = top;
				top = calloc(1, sizeof(struct cmd_line));
				cur->pipe = top;
				flag = set_argv(argv, tok, &argc);
				if (strlen(tok)==0)
				{
					printf("Syntax error!\n");
					if (flag > 0)
						while ((c=getchar())!=EOF && c!='\n') {}
					bot->argv[0][0] = 0;
					return bot;
				}
			}
			else
				flag = set_argv(argv, tok, &argc);
		}
		while (flag > 0);

	top->argv = calloc(argc+1, sizeof(char*));
	memcpy(top->argv, argv, (argc+1)*sizeof(char*));

	if (tok[strlen(tok)-1] == '&')
	{
		top->argv[argc-1][strlen(tok)-1] = 0;
		if (strlen(tok) == 1)
		{
			free(top->argv[argc-1]);
			top->argv[argc-1] = NULL;
		}
		set_all_bkg(bot);
	}
	return bot;
}

void exec_cmd(struct cmd_line *tok_line)
{
	int fd;
	if (strcmp(tok_line->argv[0], "exit")==0)
		exit(0);
	if (strcmp(tok_line->argv[0], "cd")==0)
	{	
		if (tok_line->argv[1] != NULL && chdir(tok_line->argv[1]) != 0)
			printf("shell: cd: %s: No such file or directory\n", tok_line->argv[1]);
		exit(0);
	}
	else
	{
		if (tok_line->inf != NULL)
		{
			fd = open(tok_line->inf, O_RDONLY);
			if (dup2(fd, 0)<0) exit(EXIT_FAILURE);
		}
		if (tok_line->ouf != NULL)
		{	
			fd = open(tok_line->ouf, O_WRONLY | O_CREAT, S_IWUSR | S_IRUSR);
			if (dup2(fd, 1)<0) exit(EXIT_FAILURE);
		}
		execvp(tok_line->argv[0], tok_line->argv);
		printf("shell: cd: %s: No such file or directory\n", tok_line->argv[0]);
		exit(EXIT_FAILURE);
	}
}

void handle_sigchld(int sig)
{
	int pid;
 	while ((pid = waitpid((pid_t)(-1), 0, WNOHANG)) > 0)
		sprintf(msg_buf + strlen(msg_buf), "Process %d has terminated.\n", pid);
}

void reaping_zombies()
{
	struct sigaction sa;
	sa.sa_handler = &handle_sigchld;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
	if (sigaction(SIGCHLD, &sa, 0) == -1)
	{
		perror(0);
		exit(1);
	}
}

void exec_line(struct cmd_line *tok_line)
{
	if (!tok_line->argv[0] || strlen(tok_line->argv[0])==0)
		return;
	if (strcmp(tok_line->argv[0], "exit")==0)
		exit(0);
	if (strcmp(tok_line->argv[0], "cd")==0)
	{
		if (tok_line->argv[1] != NULL && chdir(tok_line->argv[1]) != 0)
			printf("shell: cd: %s: No such file or directory\n", tok_line->argv[1]);
		return;
	}

	int cpid = fork();
	if (cpid == 0) // child
	{
		while (tok_line->pipe)
		{
			int pdes[2];
			if (pipe(pdes) == -1)
				exit(EXIT_FAILURE);
			cpid = fork();
			if (cpid == 0) // left
			{
				close(pdes[0]);
				if (dup2(pdes[1], 1) < 0) exit(EXIT_FAILURE);
				break;
			}
			else // right
			{
				waitpid(cpid, NULL, 0);
				tok_line = tok_line->pipe;
				close(pdes[1]);
				if (dup2(pdes[0], 0) < 0) exit(EXIT_FAILURE);
			}
		}
		exec_cmd(tok_line);
	}
	else if (!tok_line->bkg)
		waitpid(cpid, NULL, 0);
}

int main()
{
	struct cmd_line *line;

	while ((line = get_tok_line()) != NULL)
	{
		exec_line(line);
		destructor(line);
		reaping_zombies();
	}
	return 0;
}
