#MyShell - A simple Linux shell

A course project of Operating System.

##Build

When you are under the root directory of this project, you can build it by using the following command.

    make
    
Or you can use debug mode if you need.

    make debug
    
And delete the compiled files by

    make clean
    
##Run and Debug

To start up the shell,

    ./shell
    
Also, you can use `gdb` to debug if you have compiled it in debug mode

    gdb shell
    
##Usage
    
Just like other Linux shell, you can execute the programs which are under the current directory or those in the environment variable `PATH`. Also, you can use the command `cd` to change the current working directory. When you want to exit the shell, you can either input an `EOF`(Ctrl-D) or use the command `exit`.

A limited part of usage of doublequote (`"`) and backslash (`\`) are supported by MyShell. For example, the following commands are OK.

    mkdir "a a"
    cd a\ a
    gcc 1.c -lm -o \
        1 -g
    "make"
    
Following meta-characters are also supported, which are the basic requirements of this project.

* Input redirection (`<`)
* Output redirection (`>`)
* Connection (`|`)
* Background execution (`&`)

##Design and Implementation

This project can be devided into 3 parts. One of which is input part, another is the execution part, and then, the recycling part.

The input part is about to collect and organize tokens, check the syntax errors and return a structured command line to the second part.

The execution part receives the command line from the first part, checks whether the file exists or not and creates a subprocess for execution. In the subprocess, it will check the pipe sign. If any, the command line will be expanded into several commands, each command will in the unique subprocess which is created, waited and handled by the process that belongs to the command on the right neighbor.

The recycling part is responsible to reap the zombie processes and release the dynamically allocated memories. When a background process terminates, the main process will collect it and send a message to the console. The message forms like `Process PID has terminated.`
