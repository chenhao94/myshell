all: shell.c
	gcc shell.c -lm -O3 -Wall -o shell

debug: shell.c
	gcc shell.c -lm -Wall -o shell -g

clean:
	rm shell
